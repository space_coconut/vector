#include <vector13.h>

template <class T>
Vector<T>::Vector(){
    Size=0;
    data=new T[Size];
}

template <class T>
Vector<T>::Vector(size_t S){
    Size=S;
    data=new T[Size];
}

template <class T>
size_t Vector<T>::size(){
    return Size;
}

template <class T>
T Vector<T>::get_element(size_t index){
    return data[index];
}

template <class T>
void Vector<T>::set_element(size_t index, T value){
    data[index]=value;
}

template <class T>
T&Vector<T>::operator[](size_t index){
    return data[index];
}

template <class T>
void Vector<T>::clear(){
    delete[]data;
    Size=0;
}

template <class T>
void Vector<T>::push_back(T value){
    Size++;
    T*buffer=new T[Size];
    for (size_t i=0; i<Size; i++)
    {
        if(i<Size-1){
            buffer[i]=data[i];
        }
        else buffer[i]=value;
    }
    delete[]data;
    data=new T[Size];
    data=buffer;
}

template <class T>
void Vector<T>::push_front(T value){
    Size++;
    T*buffer=new T[Size];
    for (size_t i=0; i<Size; i++)
    {
        if(i==0){
            buffer[i]=value;
        }
        else buffer[i]=data[i-1];
    }
    delete[]data;
    data=new T[Size];
    data=buffer;
}

template <class T>
void Vector<T>::pop_back(){
    if (Size>0){
        Size--;
        T*buffer=new T[Size];
        for (size_t i=0; i<Size; i++)
             buffer[i]=data[i];
        delete[]data;
        data=new T[Size];
        data=buffer;
    }
}

template <class T>
void Vector<T>::pop_front(){
    if (Size>0){
        Size--;
        T*buffer=new T[Size];
        for (size_t i=0; i<Size; i++)
             buffer[i]=data[i+1];
        delete[]data;
        data=new T[Size];
        data=buffer;
    }
}

template <class T>
void Vector<T>::show(){
    for(size_t i=0; i<Size; i++)
        std::cout<<data[i]<<" ";
}

template <class T>
void Vector<T>::shells_sort(){
    T temp;
    size_t N=Size;
    for(size_t k=N/2; k>0; k/=2)
        for(size_t i=k; i<N; i++)
        {
            temp=data[i];
            size_t j;
            for(j=i; j>=k; j-=k)
            {
                if(temp<data[j-k])
                    data[j]=data[j-k];
                else break;
            }
        data[j]=temp;
        }
}






