#include<iostream>
#include<time.h>

#include "vector13.h"
#include "vector13.cpp"

int main()
{
    srand( time(NULL));
    Vector <int> V(8);
    for(size_t i=0;i<V.size();i++){
        V[i]=rand()%10;
    }

    V.show();

    V.push_back(99);
    V.push_front(11);
    V.push_back(99);
    V.push_front(11);
    V.pop_back();
    V.pop_front();
    std::cout<< std::endl << "Vector size: " << V.size() << std::endl;

    V.show();

    std::cout<< std::endl << "Sort vector: ";

    V.shells_sort();

    V.show();



    return 0;
}
