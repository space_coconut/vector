#ifndef VECTOR13_H
#define VECTOR13_H

#include <iostream>

template <class T>

class Vector {
    T*data;
    size_t Size;

public:
    Vector();
    Vector(size_t S);

    size_t size();
    T get_element(size_t index);
    void set_element(size_t index, T value);

    T&operator[](size_t index);

    void clear();

    void push_back(T value);

    void push_front(T value);

    void pop_back();

    void pop_front();

    void show();

    void shells_sort();
};



#endif // VECTOR13_H
